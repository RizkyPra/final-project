@extends('layouts.master')

@section('content')
<div class="container">
<div class="row">
    <div class="col-sm-12">
        <div class="post-title" style="text-align: center">
            {{$article->title}} <br>
        </div>
        <div class="post-body mt-4" style="text-align: center">
        	Dibuat : {{$article->created_at}} <br> Diperbarui : {{$article->updated_at}} <br><br>
        </div>
        <div class="post-body mt-4">
            {!!$article->body!!}  
        </div>
    </div>
</div>
</div>

@endsection