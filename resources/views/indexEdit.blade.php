@extends('layouts.master')

@section('content')
<div class="container">
<div class="row">
    <div class="col-sm-12">
        @foreach ($article as $article)
        <div class="post-content">           
            <a class="post-title" href="{{route('site.post', $article->slash)}}">
                {{$article->title}}
            </a>
            <div style="float: right">
                <a href="/{{$article->id}}/edit">
                 <button class="btn btn-success btn-sm"><i class="fas fa-edit text-white"></i></button></a>
                <form action="/{{$article->id}}" method="post" style="display:inline">
                    @csrf
                    @method('DELETE')
                    <!-- <input type="submit" value="delete"> -->
                    <button class="btn btn-danger btn-sm"><i class="fas fa-trash-alt text-white"></i></button>
                </form>
            </div>
            <div class="date-created">
                {{$article->created_at->format('d M Y')}}
            </div>
            <div class="post-body">
                {!!substr($article->body, -140)!!}
            </div>
            <div class="name-author">
                Author {{$article->user->name}}
            </div>
        </div>
    @endforeach
    </div>
    
</div>
</div>

@endsection